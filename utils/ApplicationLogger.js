const winston = require('winston');
const format = winston.format;
const logger = winston.createLogger({
    format: format.combine(
        format.timestamp(),
        format.simple(),
        format.colorize({all: true}),
    ),
    transports: [
        new winston.transports.Console()
    ]
});

module.exports.logDebug = function(callingFile,callingMethod, message) {
    logger.log('debug', message, {
        callingFile: callingFile,
        callingMethod
    })
};

module.exports.logInfo = function(callingFile,callingMethod, message) {
    logger.log('info', message, {
        callingFile: callingFile,
        callingMethod
    })
};

module.exports.logWarning = function(callingFile,callingMethod, message) {
    logger.log('warn', message, {
        callingFile: callingFile,
        callingMethod
    })
};

module.exports.logError = function(callingFile,callingMethod, message) {
    logger.log('error', message, {
        callingFile: callingFile,
        callingMethod
    })
};