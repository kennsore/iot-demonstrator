# Basic IoT System
This code is intended to test the basic concepts of a IoT system. It contains code for sending simulated sensordata to mqtt server. The server stores these data in a Influx databse srunning in its own docker container. The data can then be viewed in grafana, running in it's own separate Docker container.

## Prerequisites
- Install the latest version of Nodejs
- Install the latest version of Docker


## Setting up the system
- Clone the repository from
- Run npm install
- Ensure that you have Docker installed
- Open terminal and run the commands:
    - cd docker
    - docker-compose up

To run run the simulator first start the Server using:
- node ./server/MqttServer.js

Then start the simulated instruments using:
- node ./simulator/RunSimulator.js

## Viewing data
To view the data:
- Open a browser
- Open the address: http://localhost:3000
- Log in using user: admin password: admin
- Click the dashboard to view the data