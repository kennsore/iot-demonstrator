const Influx = require('influx');
const settings = require('./config/server_settings').influx;
const logger = require('../utils/ApplicationLogger');

function InfluxController() {
    const influx = new Influx.InfluxDB({
        host: settings.host,
        database: settings.database,
        schema: [
            {
                measurement: 'measurements',
                fields: {
                    time: Influx.FieldType.INTEGER,
                    key: Influx.FieldType.STRING,
                    value: Influx.FieldType.FLOAT,
                    unit: Influx.FieldType.STRING
                },
                tags: [
                    'host'
                ]
            }
        ]
    });

    function writeValues(intrument, time, key, value, unit, callback){
        influx.writePoints([{
            measurement: 'measurements',
            tags: {host: intrument},
            fields: {
                time: time,
                key: key,
                value: value,
                unit: unit
            }
        }]).then(result => {
            logger.logInfo('InfluxController', 'writeValues', result);
            callback(null, 'Successfully wrote to Influxdb!');
        }).catch(error => {
            logger.logError('InfluxController', 'writeValues', error);
            callback(error, null);
        })
    }

    function getDatabaseNames(callback) {
        influx.getDatabaseNames().then(names=> {
            callback(names);
        })
    }

    return {
        getDatabaseNames: getDatabaseNames,
        writeValues: writeValues
    }
}

module.exports.InfluxController = InfluxController;
