const mosca = require('mosca');
const _ = require('underscore');
const serverSetings = require('./config/server_settings');
const logger = require('../utils/ApplicationLogger');
const InfluxController = require('./InfluxController').InfluxController;
const moment = require('moment');

const influxController = new InfluxController();

const server = mosca.Server({port: serverSetings.mqtt.port});
server.authenticate = authenticateClient;

server.on('ready', () => {
    logger.logInfo('MqttServer','** on ready **','Waiting for clients to connect');
});

server.on('clientConnected', client => {
    logger.logInfo('MqttServer','** on clientConnected **','Client ' + client.id + ' is trying to connect');
});

server.on('published', (packet, client) => {
    if(packet.topic.indexOf('/new/clients') > 0) {
        logger.logInfo('MqttServer', '** on published **', 'Received a new client');
    }
    else if (packet.topic.indexOf('d:uxbhi3:Sensors') > 0) {
        logger.logInfo('MqttServer', '** on published **', 'Received a new clientId');
    }
    else if(!client) {
        logger.logWarning('MqttServer', '** on published **', 'Disconnect event');
    }
    else {
        logger.logInfo('MqttServer', '** on published **', 'Content: ' + packet.payload.toString() + ' Client: ' + client.id);
        var clientId = client.id;
        var timestamp = moment().unix();
        var value = JSON.parse(packet.payload.toString()).temperature;
        influxController.writeValues(clientId, timestamp, 'temperature', value, 'C', (error, result) => {
            if(error) {
                logger.logError('MqttServer', '** writeValues **', error);
            }
            else {
                logger.logInfo('MqttServer', '** writeValues **', result);
            }
        })
    }
});

server.on('clientDisconnected', client => {
    logger.logInfo('MqttServer', '** on clientDisconnected', client.id + ' disconnected');
})

function authenticateClient(client, username, password, done) {
    const validClients = require('./config/valid_sensors').clients;
    const pair = _.filter(validClients, c => {
        return c.clientId == (client.id);
    });
    if(pair.length === 0) {
        logger.logWarning('MqttServer', 'authenticateClient', 'Device ' + client + 'not autorized to send data');
        done('Not authorized', false);
    }
    else {
        logger.logInfo('MqttServer', 'authenticateClient', 'Device ' + client + ' autorized to send data');
        done(null, pair[0].token == password);
    }
}