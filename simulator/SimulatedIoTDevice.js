const mqtt = require('mqtt');
const logger = require('../utils/ApplicationLogger');

const val_max = 0.700;
const val_min = -0.700;


function SimulatedIoTDevice(
    instrumentName,
    host,
    clientId,
    username,
    password,
    topic,
    loggingInterval,
    startTemperature)
{
    const options = {
        clientId: clientId,
        username: username,
        password: password
    };
    var client;
    var simulatorTimer;
    var timesCalled;
    var temperature = startTemperature || 29.4;

    function connectDevice(callback) {
        client = mqtt.connect(host, options);

        client.on('error', error => {
            logger.logError('SimulatedIoTDevice', 'connectDevice', error);
            callback(error, null);
        })
        client.on('connect', () => {
            logger.logInfo('SimulatedIoTDevice', 'connectDevice', 'Connected successfully');
            callback(null, 'Connected successfully');
        })
    }

    function sendMeasurement(message) {
        logger.logDebug('SimulatedIoTDevice', 'sendMeasurement', message);
        client.publish(topic, message);
    }

    function startSimulator(callback) {
        logger.logDebug('SimulatedIoTDevice', 'startSimulator', 'Starting simulator for instrument: ' + instrumentName);
        connectDevice((error, result) => {
            if(!error) {
                logger.logDebug('SimulatedIoTDevice', 'startSimulator', 'Successfully started simulator for ' + instrumentName);
                simulatorTimer = setInterval(runSimulatorStep, loggingInterval);
                timesCalled = 0;
                callback(null, result);
            }
            else {
                logger.logError('SimulatedIoTDevice', 'startSimulator', error);
                stopSimulator();
            }
        })
    }

    function stopSimulator() {
        if(simulatorTimer) {
            clearInterval(simulatorTimer);
            simulatorTimer = null;
        }
    }

    function runSimulatorStep() {
        var data = {
            temperature: getRandomTemp(temperature)
        }
        sendMeasurement(JSON.stringify(data));
        logger.logInfo('SimulatedIoTDevice', 'runSimulatorStep', 'Successfully sent simulated data');
    }

    function getRandomTemp(setpoint) {
        var min = setpoint + val_min;
        var max = setpoint + val_max;

        return Math.random() * (max - min) + min;
    }

    return {
        connectDevice: connectDevice,
        sendMeasurement: sendMeasurement,
        startSimulator: startSimulator,
        instrumentName: instrumentName
    }
}

module.exports.SimulatedIoTDevice = SimulatedIoTDevice;
