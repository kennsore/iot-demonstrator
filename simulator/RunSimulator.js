const async = require('async');
const SimulatedDevice = require('./SimulatedIoTDevice').SimulatedIoTDevice;
const sensors = require('./config/simulated_sensors').sensors;
const logger = require('../utils/ApplicationLogger');

var activeSensors = [];

async.every(sensors, (sensor, callback) => {
    logger.logInfo('Run','','Starting simulated device :' + sensor.name);
    let dev = new SimulatedDevice(
        sensor.name,
        sensor.host,
        sensor.clientId,
        sensor.username,
        sensor.password,
        sensor.topic,
        sensor.interval,
        sensor.startTemperature
    );
    dev.startSimulator((error, result) => {
        if(!error) {
            logger.logInfo('RunSimulator', 'startSimulator -> call', 'successfully started simulator');
            activeSensors.push()
            callback(null, result);
        }
        else {
            callback(error, null);
        }
    })

}, (error, result) => {
    if(error) {
        logger.logError('RunSimulators', '** callback **',error);
    }
    else {
        logger.logInfo('RunSimulators', '** callback **',result);
    }
});